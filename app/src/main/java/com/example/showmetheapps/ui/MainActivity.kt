package com.example.showmetheapps.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.showmetheapps.R
import com.example.showmetheapps.data.PlayStoreRepository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var adapter: PlayStoreAdapter
    var repository: PlayStoreRepository = PlayStoreRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindUi()
    }

    private fun bindUi() {
        adapter = PlayStoreAdapter(repository.fetchPlayStoreApps())
        rvPlayStore.adapter = adapter
    }

}
