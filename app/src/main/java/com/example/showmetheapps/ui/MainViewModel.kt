package com.example.showmetheapps.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.showmetheapps.data.PlayStoreRepository
import com.example.showmetheapps.model.PlayStoreApp

class MainViewModel(
) : ViewModel() {

    var playStoreRepository : PlayStoreRepository = PlayStoreRepository()
    lateinit var playStoreRepoData : MutableLiveData<List<PlayStoreApp>>

    init {
        Log.d("---testing" , "MainViewModel: Init")
        getDataFromRepo()
    }

   private fun getDataFromRepo() {
       Log.d("---testing" , "MainViewModel: getDataFromRepo")
        playStoreRepoData.postValue(playStoreRepository.fetchPlayStoreApps())
    }

//    fun getPlayStoreData() : LiveData<List<PlayStoreApp>>{
//        return playStoreRepoData
//    }

}