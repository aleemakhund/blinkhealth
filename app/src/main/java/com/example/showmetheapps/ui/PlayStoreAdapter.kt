package com.example.showmetheapps.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.showmetheapps.R
import com.example.showmetheapps.model.PlayStoreApp
import kotlinx.android.synthetic.main.play_store_item.view.*


class PlayStoreAdapter(
    val playStoreItemList : List<PlayStoreApp>
) : RecyclerView.Adapter<PlayStoreAdapter.PlayStoreItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayStoreItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.getContext())
        val view: View = layoutInflater.inflate(R.layout.play_store_item, parent, false)
        return PlayStoreItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return playStoreItemList.size
    }

    override fun onBindViewHolder(holder: PlayStoreItemViewHolder, position: Int) {
        val playStoreItem  = playStoreItemList[position]
        holder.bindView(playStoreItem)
    }

    class PlayStoreItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(playStoreItem : PlayStoreApp) {
            itemView.tvAuthor.text = playStoreItem.author
            itemView.tvDescription.text = playStoreItem.description
            itemView.tvName.text = playStoreItem.name
        }
    }
}