package com.example.showmetheapps.data

import com.example.showmetheapps.model.PlayStoreAppFactory

class PlayStoreRepository {

    private val appFactory = PlayStoreAppFactory()

    fun fetchPlayStoreApps() = appFactory.createApps()

}