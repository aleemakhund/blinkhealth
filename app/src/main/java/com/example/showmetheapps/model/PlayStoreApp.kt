package com.example.showmetheapps.model

data class PlayStoreApp(
    val name: String,
    val author: String,
    val description: String?,
    val rating: Float //Any float 0.0 - 5.0
)