package com.example.showmetheapps.model

class PlayStoreAppFactory {

    fun createApps() = listOf(
        PlayStoreApp(
            "Reddit",
            "Steve Huffman",
            "Welcome to the front page of the internet.",
            4.8f
        ),
        PlayStoreApp(
            "Tik Toc",
            "Tik Toc Inc",
            "TikTok is THE destination for mobile videos. " +
                    "On TikTok, short-form videos are exciting, spontaneous," +
                    " and genuine. Whether you’re a sports fanatic, a pet enthusiast," +
                    " or just looking for a laugh, there’s something for everyone on TikTok.",
            3.5f
        ),
        PlayStoreApp(
            "Minecraft",
            "Mojang",
            null,
            4.3f
        ),
        PlayStoreApp(
            "Instagram",
            "Instagram",
            "Connect with friends, share what you’re up to, or see what's " +
                    "new from others all over the world. Explore our community where " +
                    "you can feel free to be yourself and share everything from your daily" +
                    " moments to life's highlights.",
            4.4f
        ),
        PlayStoreApp(
            "TurboTax Tax Return App – Max Refund Guaranteed",
            "Intuit",
            "Snap. Tap. Done. Snap a photo of your W-2 or 1099-MISC, " +
                    "answer simple questions about your life, and e-file securely from " +
                    "your mobile device. Free to start, pay only when you file. TurboTax" +
                    " coaches you every step of the way and automatically double-checks" +
                    " as you go, so you can be confident your taxes are done right. " +
                    "Join the millions who file taxes with the TurboTax mobile app – " +
                    "free to file for simple tax returns",
            4.7f
        ),
        PlayStoreApp(
            "Candy Crush Saga",
            "King",
            null,
            4.6f
        ),
        PlayStoreApp(
            "Kingdom Rush",
            "Ironhide Game Studio",
            null,
            4.8f
        ),
        PlayStoreApp(
            "Cash App",
            "Square",
            "Cash App is the easiest way to send, spend, save, " +
                    "and invest your money. It’s the SAFE, FAST, and FREE money app.",
            4.2f
        ),
        PlayStoreApp(
            "Tinder",
            "Tinder",
            "Tinder is the world’s most popular app for singles: " +
                    "Fill your days with new experiences, and make every single moment count.",
            3.7f
        ),
        PlayStoreApp(
            "Johnny Trigger",
            "SayGames",
            "Do you have what it takes to take down the underground world of mafia?\n" +
                    "Less talk, more bullet.",
            4.1f
        ),
        PlayStoreApp(
            "Gardenscapes",
            "Playrix",
            null,
            4.6f
        ),
        PlayStoreApp(
            "NBA 2K20",
            "2k Inc",
            "Take classic 2K action on the go with NBA 2K20 for mobile, " +
                    "featuring 5 new NBA Stories, a new MyCAREER storyline, and an " +
                    "all-new Run The Streets mode! From 5-on-5 basketball with current " +
                    "or all-time great NBA teams to streetball in Blacktop, NBA 2K20 is" +
                    " filled with a variety of game modes for all players.",
            4.0f
        ),
        PlayStoreApp(
            "Stardew Valley",
            "Chucklefish Limited",
            "Move to the countryside, and cultivate a new life in this award-winning" +
                    " open-ended farming RPG! With over 50+ hours of gameplay content and new " +
                    "Mobile-specific features, such as auto-save and multiple controls options.",
            4.9f
        ),
        PlayStoreApp(
            "Zombie Tower Defense",
            "Chess",
            null,
            3.4f
        )
    )
}